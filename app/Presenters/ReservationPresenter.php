<?php

namespace App\Presenters;

use App\Model\Reservation\ReservationRepository;

class ReservationPresenter extends AbstractPresenter
{

    private ReservationRepository $reservationsRepository;

    public function __construct(ReservationRepository $reservationsRepository)
    {
        $this->reservationsRepository = $reservationsRepository;
        parent::__construct();
    }

    public function renderDefault()
    {

        $this->template->reservations = $this->reservationsRepository->getAll();

    }

    public function actionDetail(?int $id = null){
        if ($id === null){
            $this->redirect(destination: 'Reservation:default');
        }
    }

    public function renderDetail(int $id): void
    {
        $this->template->reservation = $this->reservationsRepository->getById($id);
    }
}