<?php

namespace App\Model\Reservation;

use JetBrains\PhpStorm\Pure;
use Nette\Database\Row;

class ReservationMapper
{
    #[Pure] public function mapFromDatabaseRow(Row $row): Reservation
    {
            return new Reservation((int)$row->id, $row->termFrom, $row->termTo, (string)$row->name, (string)$row->address, (string)$row->phone, (string)$row->email, (string)$row->agency, (string)$row->text, (string)$row->price, (string)$row->emailDate);
    }
}