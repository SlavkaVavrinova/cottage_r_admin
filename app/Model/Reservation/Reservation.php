<?php

namespace App\Model\Reservation;

class Reservation
{

    private int $id;
    private \DateTime $termFrom;
    private \DateTime $termTo;
    private string $name;
    private string $address;
    private string $phone;
    private string $email;
    private string $agency;
    private string $text;
    private string $price;
    private string $emailDate;

    public function __construct(int $id, \DateTime $termFrom, \DateTime $termTo, string $name, string $address, string $phone, string $email, string $agency, string $text, string $price, string $emailDate)
    {
        $this->id = $id;
        $this->termFrom = $termFrom;
        $this->termTo = $termTo;
        $this->name = $name;
        $this->address = $address;
        $this->phone = $phone;
        $this->email = $email;
        $this->agency = $agency;
        $this->text = $text;
        $this->price = $price;
        $this->emailDate = $emailDate;
    }

       public function getId(): int
    {
        return $this->id;
    }

    public function getTermFrom(): \DateTime
    {
        return $this->termFrom;
    }

        public function getTermTo(): \DateTime
    {
        return $this->termTo;
    }

        public function getName(): string
    {
        return $this->name;
    }

        public function getAddress(): string
    {
        return $this->address;
    }

      public function getPhone(): string
    {
        return $this->phone;
    }

        public function getEmail(): string
    {
        return $this->email;
    }

      public function getAgency(): string
    {
        return $this->agency;
    }

       public function getText(): string
    {
        return $this->text;
    }

        public function getPrice(): string
    {
        return $this->price;
    }

        public function getEmailDate(): string
    {
        return $this->emailDate;
    }


}