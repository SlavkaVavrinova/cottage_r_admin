<?php
declare(strict_types=1);

namespace App\Model\Reservation;

use Nette\Database\Connection;
use Nette\Database\Row;

class ReservationRepository
{

    private Connection $connection;
    private ReservationMapper $reservationMapper;

    public function __construct(
        Connection $connection,
    ReservationMapper $reservationMapper) {
        $this->connection = $connection;
        $this->reservationMapper = $reservationMapper;
    }

    public function getAll(): array
    {
        $reservationsFromDatabase = $this->connection->query('SELECT id, termFrom, termTo, name, address,phone,email,agency,text,price, emailDate FROM ruzenka')->fetchAll();

        $reservations = [];
/** @var Row[] $reservationDatabaseRow */
        foreach ($reservationsFromDatabase as $reservationDatabaseRow) {


            $reservations[] = $this->reservationMapper->mapFromDatabaseRow($reservationDatabaseRow);
}
        return $reservations;
    }

    public function getById(int $id):Reservation
    {
        $reservationFromDatabase = $this
            ->connection
            ->query('SELECT id, termFrom, termTo, name, address,phone, email,agency,text,price, emailDate FROM ruzenka WHERE id=?', $id)
            ->fetch();
       $reservation = $this->reservationMapper->mapFromDatabaseRow($reservationFromDatabase);
       return $reservation;
    }
}